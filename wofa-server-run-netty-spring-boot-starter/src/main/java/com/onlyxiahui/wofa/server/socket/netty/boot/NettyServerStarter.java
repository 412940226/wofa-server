package com.onlyxiahui.wofa.server.socket.netty.boot;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.onlyxiahui.framework.action.dispatcher.general.GeneralMessageHandler;
import com.onlyxiahui.wofa.server.net.core.session.SocketSessionHandler;
import com.onlyxiahui.wofa.server.socket.netty.config.NetServerProperties;

/**
 *
 * Description 容器启动后要初始化服务<br>
 * Date 2020-08-06 20:53:17<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(NetServerProperties.class)
@ConditionalOnBean({ GeneralMessageHandler.class, SocketSessionHandler.class })
public class NettyServerStarter {

	@Bean
	public NettyServerRunner nettyServerRunner() {
		NettyServerRunner bean = new NettyServerRunner();
		return bean;
	}
}
