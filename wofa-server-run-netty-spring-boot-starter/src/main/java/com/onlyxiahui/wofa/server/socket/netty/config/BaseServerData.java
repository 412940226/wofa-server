package com.onlyxiahui.wofa.server.socket.netty.config;

/**
 * Description <br>
 * Date 2021-01-08 10:48:27<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class BaseServerData {

	private SslData ssl;

	public SslData getSsl() {
		return ssl;
	}

	public void setSsl(SslData ssl) {
		this.ssl = ssl;
	}
}
