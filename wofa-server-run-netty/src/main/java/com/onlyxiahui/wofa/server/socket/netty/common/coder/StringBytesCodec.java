package com.onlyxiahui.wofa.server.socket.netty.common.coder;

import io.netty.channel.CombinedChannelDuplexHandler;

public class StringBytesCodec extends CombinedChannelDuplexHandler<StringBytesDecoder, StringBytesEncoder> {
	public StringBytesCodec() {
		super(new StringBytesDecoder(), new StringBytesEncoder());
	}
}
