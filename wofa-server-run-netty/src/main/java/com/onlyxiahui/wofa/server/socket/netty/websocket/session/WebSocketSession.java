package com.onlyxiahui.wofa.server.socket.netty.websocket.session;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.UUID;

import com.onlyxiahui.framework.net.session.AbstractSession;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;

public class WebSocketSession extends AbstractSession {

	String id;

	ChannelHandlerContext context;

	public WebSocketSession(ChannelHandlerContext context) {
		this.context = context;
		this.id = UUID.randomUUID().toString().replace("-", "");
	}

	@Override
	public void write(Object object) {
		if (null != object && null != context) {
			Channel channel = context.channel();
			if (channel.isOpen()) {
				if (object instanceof String) {
					channel.writeAndFlush(new TextWebSocketFrame(object.toString()));
				}
			}
		}
	}

	@Override
	public void close() {
		ChannelFuture cf = context.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN));
		cf.addListener(ChannelFutureListener.CLOSE);
		// context.close();
	}

	@Override
	public String getRemoteAddress() {
		Channel channel = context.channel();
		String address = "";

		AttributeKey<String> remoteAddressKey = AttributeKey.valueOf("remote_address_key");

		Attribute<String> attribute = channel.attr(remoteAddressKey);
		String value = attribute.get();
		if (null != value) {
			address = value;
		} else {
			SocketAddress socketAddress = channel.remoteAddress();
			if (socketAddress instanceof InetSocketAddress) {
				InetSocketAddress sa = (InetSocketAddress) socketAddress;
				address = sa.getHostString();
			} else {
				String temp = socketAddress.toString();
				if (temp != null) {
					String[] array = temp.replace("/", "").split(":");
					address = array[0];
				}
			}
		}
		return address;
	}

	@Override
	public String getId() {
		return context.channel().id().asLongText();
	}
}
