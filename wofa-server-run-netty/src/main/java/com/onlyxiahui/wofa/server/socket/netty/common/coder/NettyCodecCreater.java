package com.onlyxiahui.wofa.server.socket.netty.common.coder;

import io.netty.channel.ChannelHandler;

/**
 * Description <br>
 * Date 2020-11-30 17:04:15<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface NettyCodecCreater {

	ChannelHandler get();
}
