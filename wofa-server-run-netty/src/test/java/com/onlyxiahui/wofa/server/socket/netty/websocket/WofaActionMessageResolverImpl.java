package com.onlyxiahui.wofa.server.socket.netty.websocket;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.action.dispatcher.ActionDispatcher;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionMessage;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.general.extend.ActionMessageResolver;
import com.onlyxiahui.framework.action.dispatcher.general.util.ActionDispatcherJsonUtil;

/**
 * Description <br>
 * Date 2020-06-08 15:31:02<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class WofaActionMessageResolverImpl implements ActionMessageResolver {

	@Override
	public ActionMessage resolver(ActionDispatcher actionDispatcher, String path, Object data, ArgumentBox argumentBox) {
		String message = (data instanceof String) ? data.toString() : "";
		if (ActionDispatcherJsonUtil.maybeJson(message)) {
			JSONObject jo = JSONObject.parseObject(message);
			argumentBox.put(JSONObject.class, jo);
		}
		ActionMessage am = new ActionMessageImpl();
		am.setMessage(data);
		am.setAction((path == null || path.isEmpty()) ? "/all" : path);
		return am;
	}

	class ActionMessageImpl implements ActionMessage {

		Object message;
		String action;

		@Override
		public void setMessage(Object message) {
			this.message = message;
		}

		@Override
		public Object getMessage() {
			return this.message;
		}

		@Override
		public String getAction() {
			return this.action;
		}

		@Override
		public void setAction(String action) {
			this.action = action;
		}
	}
}
