
package com.onlyxiahui.wofa.server.socket.netty.websocket;

import org.springframework.stereotype.Component;

import com.onlyxiahui.framework.net.session.SocketSession;
import com.onlyxiahui.wofa.server.net.core.session.SocketSessionBox;
import com.onlyxiahui.wofa.server.net.core.session.SocketSessionHandler;
import com.onlyxiahui.wofa.server.socket.netty.common.util.JsonHandlerUtil;

/**
 * Description <br>
 * Date 2020-04-13 11:23:46<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Component
public class SocketSessionHandlerImpl implements SocketSessionHandler {

	SocketSessionBox socketSessionBox = new SocketSessionBox();

	@Override
	public void sessionPut(SocketSession socketSession) {
		// String key = socketSession.getKey();
		socketSessionBox.put(socketSession);
	}

	@Override
	public boolean sessionHas(String key) {
		return socketSessionBox.hasSession(key);
	}

	@Override
	public void sessionRemove(SocketSession socketSession) {
		String key = socketSession.getKey();
		socketSessionBox.remove(socketSession);

		if (null != key) {
			if (!sessionHas(key)) {
				// 通知下线
			}
		}
	}

	@Override
	public void dataException(SocketSession socketSession, Object data, Exception e) {
		// String message = e.getMessage();
	}

	@Override
	public void dataWrite(SocketSession socketSession, Object data) {
		if (null != socketSession && null != data) {
			socketSession.write(JsonHandlerUtil.toJson(data));
		}
	}
}
